DOCKER=docker

all: build_and_run

stop_and_rm:
	if $(DOCKER) ps -a | grep -q simple-calculator-app; \
	then $(DOCKER) stop simple-calculator-app && $(DOCKER) rm simple-calculator-app; \
	fi

build_and_run: stop_and_rm
	npm run build && $(DOCKER) build -t simple-calculator . && \
	  $(DOCKER) run -dit --name simple-calculator-app -p 8080:80 simple-calculator

build:
	npm run build && $(DOCKER) build -t simple-calculator .
