# Simple Calculator

It is a calculator that was built using CSS and TypeScript. The application can be deployed to Amazon Web Services (AWS) via Terraform. It can also be deployed as a Docker container.

## Live Demo

[Simple Calculator](https://drlg87syexil1.cloudfront.net)

## Developer Notes

### Building and Deploying to AWS Using Terraform

1. Install all the NPM packages.

```
npm install
```

2. Make all code changes in the TypeScript `index.ts` file.

3. Then generate the JavaScript source code (`index.js`) that will be included in the `index.html` file:

```
npm run build
```

4. Deploy all the files in the `app` directory to an Amazon S3 bucket using the included Terraform configuration.

```
cd terraform
terraform init
terraform plan
terraform apply
```

### Building a Docker Image

Create the image and deploy it to a container using the `make` command. Only all the contents of the `app` directory will be copied to the image.

The `make` command will use NPM to generate the JavaScript source code before building the image.

```
make
```

### Downloading the Latest Docker Image of Simple Calculator (Optional)

```
docker pull public.ecr.aws/b5i5u6l6/simple-calculator:latest
```
