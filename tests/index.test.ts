import { calculate, DivisionByZeroError, mulDivAddSub } from '../app/js/index';
import { operators, parseExpression, validate } from '../app/js/index';
import { expect, test } from '@jest/globals';

describe('Tests the Simple Calculator code', () => {
    test('Operators Map', () => {
        expect(operators.get('+')(1, 5)).toEqual(6);
        expect(operators.get('-')(2.5, 2)).toEqual(0.5);
        expect(operators.get('*')(3, 5)).toEqual(15);
        expect(operators.get('/')(10, 2)).toEqual(5);
    });

    test('MulDivAddSub Function', () => {
        expect(mulDivAddSub(['3', '*', '3', '+', '6', '+', '6', '*', '2'])).toEqual(['27']);
        expect(mulDivAddSub(['3', '+', '27', '/', '3'])).toEqual(['12']);
        expect(mulDivAddSub(['25', '-', '30', '+', '5'])).toEqual(['0']);
    });

    test('Calculate Function', () => {
        expect(calculate(['3', '*', '(', '3', '+', '5', '-', '2', ')', '/', '2']))
            .toEqual(['9']);
        expect(calculate(['3', '*', '(', '3', '+', '(', '8', '*', '2', ')', '-', '5', '-', '2',
            ')', '/', '2'])).toEqual(['18']);
        expect(calculate(['(', '3', '+', '2', ')'])).toEqual(['5']);
        expect(calculate(['(', '(', '2', '-', '4', ')', '+', '5', ')'])).toEqual(['3']);
        expect(() => { calculate(['4', '/', '(', '25', '-', '30', '+', '5', ')']); })
            .toThrow(new DivisionByZeroError('Division by zero!'));
        expect(calculate(['5', '/', '5', '+', '4', '-', '3', '*', '-3'])).toEqual(['14']);
    });

    test('Validate Function with Invalid Test Cases', () => {
        // empty expression
        expect(validate([]).length).toBeGreaterThan(0);

        // number of opening parentheses and number of closing parentheses are different
        expect(validate(['3', '(', '3', '+', '5', '-', '2']).length).toBeGreaterThan(0);

        // number immediately precedes opening parenthesis
        expect(validate(['3', '(', '3', '+', '5', '-', '2', ')', '/', '2']).length)
            .toBeGreaterThan(0);

        // closing parenthesis is present before opening parenthesis
        expect(validate([')', '3', '+', '5', '-', '2', '(', '/', '2']).length).toBeGreaterThan(0);

        // missing number on right side of "/" operator
        expect(validate(['3', '/']).length).toBeGreaterThan(0);

        // operator immediately follows opening parenthesis
        expect(validate(['(', '+', '2', ')', '-', '3']).length).toBeGreaterThan(0);

        // operator immediately precedes closing parenthesis
        expect(validate(['(', '1', '+', ')', '-', '3']).length).toBeGreaterThan(0);

        // no operand between first two numbers
        expect(validate(['3', '3', '+', '2']).length).toBeGreaterThan(0);

        // just a decimal point
        expect(validate(['3', '+', '.']).length).toBeGreaterThan(0);

        // more than one decimal point in number
        expect(validate(['3', '+', '0.25.5']).length).toBeGreaterThan(0);
    });

    test('Validate Function with Valid Test Cases', () => {
        expect(validate(['3', '+', '(', '3', '+', '5', '-', '2', ')', '/', '2'])).toEqual('');
        expect(validate(['(', '3', '+', '5', '-', '2', ')', '/', '2'])).toEqual('');
        expect(validate(['3', '+', '(', '2', '-', '1', ')'])).toEqual('');
        expect(validate(['(', '1', '+', '2', ')', '-', '3'])).toEqual('');
        expect(validate(['(', '1', '+', '2', ')', '-', '3.25'])).toEqual('');
    });

    test('ParseExpression Function', () => {
        expect(parseExpression('2 + 4 * 33 / 7')).toEqual(['2', '+', '4', '*', '33', '/', '7']);
        expect(parseExpression('2 -  -4*33 / 7')).toEqual(['2', '+', '4', '*', '33', '/', '7']);
        expect(parseExpression('5 / 5 + 4 - 3 * -3'))
            .toEqual(['5', '/', '5', '+', '4', '-', '3', '*', '-3']);
    });
});
