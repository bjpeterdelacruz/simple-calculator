/* eslint @typescript-eslint/no-unused-vars: "off" */

const expression: string[] = [];

class DivisionByZeroError extends Error {

}

type op = (a: number, b: number) => number;

const operators = new Map<string, op>();
operators.set('+', (a: number, b: number): number => a + b);
operators.set('-', (a: number, b: number): number => a - b);
operators.set('*', (a: number, b: number): number => a * b);
operators.set('/', (a: number, b: number): number => {
    if (b === 0) {
        throw new DivisionByZeroError('Division by zero!');
    }
    return a / b;
});

function toggleButton(enabled: boolean, button: HTMLButtonElement, className: string): void {
    if (enabled) {
        button.removeAttribute('disabled');
        button.classList.add(className);
        button.classList.remove('btn-secondary');
    }
    else {
        button.setAttribute('disabled', 'disabled');
        button.classList.remove(className);
        button.classList.add('btn-secondary');
    }
}

function parseExpression(expr: string): string[] {
    const arr = [];
    let temp = '';

    for (const char of expr.replace(/\s+/g, '').replace('--', '+').split('')) {
        if (char.match(/[-+*/()]/) !== null) {
            if (temp) {
                arr.push(temp);
            }
            arr.push(char);
            temp = '';
        }
        else {
            temp += char;
        }
    }
    if (temp) {
        arr.push(temp);
    }

    for (let idx = 0; idx < arr.length - 2; idx++) {
        if (arr[idx].match(/[-+*/]/) && arr[idx + 1].match(/[-]/)
                && arr[idx + 2].match(/[.0-9]+/)) {
            arr.splice(idx + 1, 2, `-${arr[idx + 2]}`);
        }
    }
    return arr;
}

window.onload = function(): void {
    const equalsButton = document.getElementById('btn-equals') as HTMLButtonElement;
    toggleButton(false, equalsButton, 'btn-success');

    const number = document.getElementById('number') as HTMLInputElement;
    number.value = '';
    number.focus();
    number.addEventListener('keyup', (event: Event) => {
        if (!(event instanceof KeyboardEvent) || event.key !== 'Enter') {
            return;
        }

        const preview = document.getElementById('preview') as HTMLInputElement;
        if (number.value.length === 0 && preview.value.length > 0) {
            processValue('=');
            return;
        }
        if (number.value.length === 0 && preview.value.length === 0) {
            let msg = 'Please input an expression (the part of an equation to the left of the ';
            msg += 'equal sign) and then press Enter twice to view the result. For example: ';
            msg += '11 + 23';
            alert(msg);
            return;
        }

        if (!number.value.match(/[.0-9-+*/)(]+/)) {
            alert('Only the following characters are allowed: 0 1 2 3 4 5 6 7 8 9 . - + * / ( )');
            return;
        }

        const expr = number.value;
        number.value = '';
        for (const value of parseExpression(expr)) {
            processValue(value);
        }
        number.value = '';
    });

    const preview = document.getElementById('preview') as HTMLInputElement;
    preview.value = '';

    const history = document.getElementById('history') as HTMLInputElement;
    history.value = '';

    const form = document.getElementById('form') as HTMLFormElement;
    form.addEventListener('submit', (event: Event) => event.preventDefault(), false);

    const copyrightYear = document.getElementById('copyright-year') as HTMLSpanElement;
    copyrightYear.innerHTML = new Date().getFullYear().toString();
};

function resetInputField(): void {
    const number = document.getElementById('number') as HTMLInputElement;
    number.value = '';

    expression.pop();
    const preview = document.getElementById('preview') as HTMLInputElement;
    preview.value = expression.join(' ');

    if (expression.length === 0) {
        const equalsButton = document.getElementById('btn-equals') as HTMLButtonElement;
        toggleButton(false, equalsButton, 'btn-success');
    }

    number.focus();
}

function resetAll(): void {
    if (!confirm('Are you sure you want to clear your history of calculations?')) {
        return;
    }

    const number = document.getElementById('number') as HTMLInputElement;
    number.value = '';

    expression.length = 0;
    const preview = document.getElementById('preview') as HTMLInputElement;
    preview.value = '';

    const history = document.getElementById('history') as HTMLInputElement;
    history.value = '';
    
    const equalsButton = document.getElementById('btn-equals') as HTMLButtonElement;
    toggleButton(false, equalsButton, 'btn-success');

    number.focus();
}

function mulDivAddSub(array: string[]): string[] {
    const mulDivIndexes: number[] = [];
    for (let idx = 0; idx < array.length; idx++) {
        if (array[idx] === '*' || array[idx] === '/') {
            mulDivIndexes.push(idx);
        }
    }
    mulDivIndexes.sort((a, b) => a - b);

    const addSubIndexes: number[] = [];
    for (let idx = 0; idx < array.length; idx++) {
        if (array[idx] === '+' || array[idx] === '-') {
            addSubIndexes.push(idx);
        }
    }
    addSubIndexes.sort((a, b) => a - b);

    const indexes = [...mulDivIndexes, ...addSubIndexes];
    if (indexes.length === 0) {
        return array;
    }

    const index = indexes[0];
    const left: number = parseFloat(array[index - 1]);
    const operand: string = array[index];
    const right: number = parseFloat(array[index + 1]);

    const operation: op | undefined = operators.get(operand);
    if (operation) {
        array.splice(index - 1, 3, operation(left, right).toString());
    }
    return mulDivAddSub(array);
}

function calculate(array: string[]): string[] {
    const copy = [...array];
    while (copy.indexOf('(') !== -1) {
        const leftParenthesisIndex = copy.lastIndexOf('(');
        const rightParenthesisIndex = copy.indexOf(')');
        const numberOfCharacters = rightParenthesisIndex - leftParenthesisIndex + 1;
        const result = mulDivAddSub(copy.slice(leftParenthesisIndex + 1, rightParenthesisIndex));
        copy.splice(leftParenthesisIndex, numberOfCharacters, result[0].toString());
    }
    return mulDivAddSub(copy);
}

/**
 * An expression is valid if and only if:
 * 
 * -- The expression is not an empty string
 * -- The number of parentheses are equal
 * -- All opening parentheses come before all closing parentheses
 * -- No numbers immediately precede an opening parenthesis
 * -- No numbers immediately follow a closing parenthesis
 * -- An opening parenthesis does not immediately precede an arithmetic operator
 * -- A closing parenthesis does not immediately follow an arithmetic operator
 * -- An arithmetic operator or a closing parenthesis immediately follows a number
 * -- A floating point number has exactly one decimal point
 * 
 * @param expr An expression that is represented as an array of strings to validate
 * @returns An error message to be displayed to the user or an empty string if the expression is
 * valid
 */
function validate(expr: string[]): string {
    if (expr.length === 0) {
        return 'Please enter an expression.';
    }

    const leftParenthesesCount = expr.filter(str => str === '(').length;
    const rightParenthesesCount = expr.filter(str => str === ')').length;

    if (leftParenthesesCount !== rightParenthesesCount) {
        return 'Number of opening and closing parentheses do not match.';
    }

    if (leftParenthesesCount > 0 && rightParenthesesCount > 0
        && expr.indexOf(')') < expr.indexOf('(')) {
        return 'Opening parenthesis must always come before closing parenthesis.';
    }

    for (const symbol of ['*', '/', '+', '-']) {
        const index = expr.indexOf(symbol);
        if (index === -1) {
            continue;
        }

        const left = expr[index - 1];
        if (left.match(/[-+*/(]+/) && left.length === 1) {
            return `A number must precede an arithmetic operator: ${expr.join(' ')}`;
        }
        if (index + 1 <= expr.length - 1) {
            const right = expr[index + 1];
            if (right.match(/[-+*/)]+/) && right.length === 1) {
                return `A number must follow an arithmetic operator: ${expr.join(' ')}`;
            }
        }
        else {
            return `Expression must not end with an arithmetic operator: ${expr.join(' ')}`;
        }
    }

    for (let idx = 0; idx < expr.length - 1; idx++) {
        if (expr[idx].match(/[.0-9]+/) && expr[idx + 1].match(/[.(0-9]+/)) {
            return 'An arithmetic operator or a closing parenthesis must come after an operand.';
        }
    }

    for (const element of expr) {
        if (element === '.' || element.split('').filter(x => x === '.').length > 1) {
            return `Not a valid number: ${element}`;
        }
    }

    return '';
}

function processValue(value: string): void {
    const number = document.getElementById('number') as HTMLInputElement;
    const preview = document.getElementById('preview') as HTMLTextAreaElement;
    const history = document.getElementById('history') as HTMLTextAreaElement;
    const equalsButton = document.getElementById('btn-equals') as HTMLButtonElement;

    if (value.match(/[.0-9]/)) {
        number.value += value;
        if (expression.length && expression[expression.length - 1].match(/[.0-9]+/)) {
            expression.pop();
        }
        expression.push(number.value);
        preview.value = expression.join(' ');
    }
    else if (value.match(/[()]/)) {
        if (value === ')' && number.value) {
            number.value = '';
        }
        expression.push(value);
        preview.value = expression.join(' ');
    }
    else if (value === '+/-') {
        if (number.value === '') {
            return;
        }
        number.value = number.value.indexOf('-') === -1 ? `-${number.value}` :
            number.value.substring(1);
        if (expression.length && expression[expression.length - 1].match(/[.0-9]+/)) {
            expression.pop();
        }
        expression.push(number.value);
        preview.value = expression.join(' ');
    }
    else if (value.match(/[-+*/]/)) {
        number.value = '';
        expression.push(value);
        preview.value = expression.join(' ');
    }
    else if (value === '=') {
        const msg = validate(expression);
        if (msg) {
            alert(msg);
            return;
        }

        try {
            history.value += `${expression.join(' ')} = ${calculate(expression)[0]}\n`;
        }
        catch (err) {
            if (err instanceof DivisionByZeroError) {
                history.value += `${expression.join(' ')} = ${err.message}\n`;
            }
        }

        number.value = '';
        expression.length = 0;
        preview.value = '';
        toggleButton(false, equalsButton, 'btn-success');
        return;
    }

    toggleButton(true, equalsButton, 'btn-success');
}

module.exports = { calculate, DivisionByZeroError, mulDivAddSub,
                   operators, parseExpression, validate };
