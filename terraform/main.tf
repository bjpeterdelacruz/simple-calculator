terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.52.0"
    }
  }
  backend "s3" {
    bucket = "dev-bjdelacruz-terraform"
    key    = "simple-calculator/state.tfstate"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

locals {
  bucket_name       = "dev-bjdelacruz-simple-calculator"
  region_name       = data.aws_region.current.name
  region_account_id = "${local.region_name}:${data.aws_caller_identity.current.account_id}"
}

resource "aws_s3_object" "upload_html_file" {
  bucket       = module.s3_static_website.bucket_id
  key          = "index.html"
  source       = "${path.module}/../app/index.html"
  content_type = "text/html"
  etag         = filemd5("${path.module}/../app/index.html")
}

resource "aws_s3_object" "upload_js_file" {
  bucket       = module.s3_static_website.bucket_id
  key          = "js/index.js"
  source       = "${path.module}/../app/js/index.js"
  content_type = "text/javascript"
  etag         = filemd5("${path.module}/../app/js/index.js")
}

resource "aws_s3_object" "upload_css_file" {
  bucket       = module.s3_static_website.bucket_id
  key          = "css/styles.css"
  source       = "${path.module}/../app/css/styles.css"
  content_type = "text/css"
  etag         = filemd5("${path.module}/../app/css/styles.css")
}

module "s3_static_website" {
  source = "git@bitbucket.org:bjpeterdelacruz/serverless-applications.git//modules/create-s3-static-website"

  s3_bucket_name                 = local.bucket_name
  origin_access_identity_iam_arn = module.cdn.origin_access_identity_iam_arn
}

module "cdn" {
  source = "git@bitbucket.org:bjpeterdelacruz/serverless-applications.git//modules/create-cdn"

  website_bucket_domain_name = module.s3_static_website.bucket_domain_name
  website_bucket_origin_id   = module.s3_static_website.bucket_id
  logging_bucket             = "simple-calculator-cloudfront-logs"
}

output "website_url" {
  value = module.cdn.website_url
}
